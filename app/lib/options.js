var APP = require("core");

exports.logout = function() {
	Ti.App.Properties.setString("sessionId", "");
	Alloy.Globals.swipeEnabled = false;
	APP.addChild('login');
};

exports.disableBack = function() {
	APP.MainWindow.addEventListener("android:back", blocker);
};

exports.enableBack = function() {
	APP.MainWindow.removeEventListener("android:back", blocker);
};

var blocker = function() {

};