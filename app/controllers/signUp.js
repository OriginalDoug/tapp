//this gets the code the ChariTi framework needs to run
var APP = require("core");

//this is used if you want to pass data from one controller to another
var CONFIG = arguments[0];

//this is the first function of the app that runs
var init = function() {
	//this tracks what the user clicks on and can be sent to us (the developers) if they run into problems
	APP.log("debug", "example.init | " + JSON.stringify(CONFIG));

	//set the background color to whatever we set in /app/lib/data/app.json or if we didn't do that, set it to black (#000) by default
	$.NavigationBar.setBackgroundColor(APP.Settings.colors.primary || "#000");

	//if this is "the child" of a nother contoller (a details screen, etc), then show the back button in the upper left
	//if(CONFIG.isChild === true) {
	$.NavigationBar.showBack();
	//if it's not a child and we are using the slide menu, show the slide menu icon in the upper left
	//if(APP.Settings.useSlideMenu) {
	//$.NavigationBar.showMenu();
	//if it's not a child and we aren't using the slide menu, show the settings icon in the navbar
	//} else {
	//$.NavigationBar.showSettings();
	//}

};

var Cloud = require('ti.cloud');
Cloud.debug = false;

var createUser = function() {
	Cloud.Users.create({
		email: $.email.value,
		first_name: $.firstName.value,
		last_name: $.lastName.value,
		username: $.userName.value,
		password: $.password.value,
		password_confirmation: $.confirm.value
	}, function(e) {
		if(e.success) {
			var user = e.users[0];
			alert('Success:\n' + 'id: ' + user.id + '\n' + 'sessionId: ' + Cloud.sessionId + '\n' + 'first name: ' + user.first_name + '\n' + 'last name: ' + user.last_name);
			APP.addChild('login');
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
};

$.signUp.addEventListener('click', function() {
	createUser();
});

init();