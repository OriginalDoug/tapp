var APP = require("core");
var OPTIONS = require("options");
//this is used if you want to pass data from one controller to another
var CONFIG = arguments[0];

//this is the first function of the app that runs
var init = function() {
	//this tracks what the user clicks on and can be sent to us (the developers) if they run into problems
	APP.log("debug", "example.init | " + JSON.stringify(CONFIG));
	OPTIONS.enableBack();
	//set the background color to whatever we set in /app/lib/data/app.json or if we didn't do that, set it to black (#000) by default
	$.NavigationBar.setBackgroundColor(APP.Settings.colors.primary || "#000");

	//if this is "the child" of a nother contoller (a details screen, etc), then show the back button in the upper left
	//if(CONFIG.isChild === true) {
	$.NavigationBar.showBack();
	//if it's not a child and we are using the slide menu, show the slide menu icon in the upper left
	//if(APP.Settings.useSlideMenu) {
	//$.NavigationBar.showMenu();
	//if it's not a child and we aren't using the slide menu, show the settings icon in the navbar
	//} else {
	//	$.NavigationBar.showSettings();
	//}
	//}
};

var Cloud = require('ti.cloud');
Cloud.debug = false;

Cloud.Objects.query({
	classname: 'filters',
	page: 1,
	per_page: 10

}, function(e) {
	if(e.success) {
		var tbl_data = [];
		for(var i = 0; i < e.filters.length; i++) {
			var row = Ti.UI.createTableViewRow({
				title: e.filters[i].name
			});
			Ti.API.info(e.filters[i].name);
			tbl_data.push(row);

		};

		$.filterTable.setData(tbl_data);

	} else {
		alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	}
});

$.NavigationBar.showRight({
	image: "/images/addIconLight.png",
	callback: function() {
		APP.addChild('addFilter');
	}
});

// Kick off the init
init();