//this gets the code the ChariTi framework needs to run
var APP = require("core");
var OPTIONS = require("options");
//this is used if you want to pass data from one controller to another
var CONFIG = arguments[0] || {};

//this is the first function of the app that runs
var init = function() {
	//this tracks what the user clicks on and can be sent to us (the developers) if they run into problems
	APP.log("debug", "example.init | " + JSON.stringify(CONFIG));
	OPTIONS.disableBack();
	//set the background color to whatever we set in /app/lib/data/app.json or if we didn't do that, set it to black (#000) by default
	//$.NavigationBar.setBackgroundColor(APP.Settings.colors.primary || "#000");
	//if this is "the child" of a nother contoller (a details screen, etc), then show the back button in the upper left
	//if(CONFIG.isChild === true) {
	//$.NavigationBar.showBack();
	//if it's not a child and we are using the slide menu, show the slide menu icon in the upper left
	//} else if(APP.Settings.useSlideMenu) {
	//$.NavigationBar.showMenu();
	//if it's not a child and we aren't using the slide menu, show the settings icon in the navbar
	//} else {
	//$.NavigationBar.showSettings();
	//}

};

var Cloud = require('ti.cloud');
Cloud.debug = false;

APP.GlobalWrapper.remove(APP.Tabs.Wrapper);
APP.ContentWrapper.bottom = "0dp";

$.submit.addEventListener('click', function() {
	login();
});

var login = function() {

	var user = $.username.value;
	var pass = $.password.value;
	Ti.API.info(user);
	Ti.API.info(pass);

	Cloud.Users.login({
		login: user,
		password: pass
	}, function(e) {
		if(e.success) {
			Ti.App.Properties.setString("sessionId", e.users[0].sessionId);
			Alloy.Globals.swipeEnabled = true;
			OPTIONS.enableBack();
			APP.GlobalWrapper.add(APP.Tabs.Wrapper);
			//Ti.API.info('APP.ContentWrapper.bottom' + APP.ContentWrapper.bottom);
			APP.ContentWrapper.bottom = "40dp";
			APP.removeAllChildren();

		} else {
			alert("Invalid username or password.");
		}
	});
};

var checkLogin = function() {
	Cloud.Users.showMe(function(e) {
		if(e.success) {
			Ti.App.Properties.setString("sessionId", e.users[0].sessionId);
			APP.removeChild();
		} else {
			alert("Invalid username or password.");
		}
	});
};

init();