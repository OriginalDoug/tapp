// Pull in the core APP singleton
var APP = require("core");

// Make sure we always have a reference to global elements throughout the APP singleton
APP.MainWindow = $.MainWindow;
APP.GlobalWrapper = $.GlobalWrapper;
APP.ContentWrapper = $.ContentWrapper;
APP.Tabs = $.Tabs;
APP.SlideMenu = $.SlideMenu;

// Start the APP
APP.init();
var Cloud = require('ti.cloud');
Cloud.debug = false;
var sessionId = Ti.App.Properties.getString("sessionId");

if(!sessionId) {
	APP.addChild("open");
} else {
	Cloud.sessionId = sessionId;
}