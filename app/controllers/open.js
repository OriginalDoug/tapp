//this gets the code the ChariTi framework needs to run
var APP = require("core");
var OPTIONS = require("options");

//this is used if you want to pass data from one controller to another
var CONFIG = arguments[0];

//this is the first function of the app that runs
var init = function() {
	//this tracks what the user clicks on and can be sent to us (the developers) if they run into problems
	APP.log("debug", "example.init | " + JSON.stringify(CONFIG));
	OPTIONS.disableBack();
	//set the background color to whatever we set in /app/lib/data/app.json or if we didn't do that, set it to black (#000) by default
	//$.NavigationBar.setBackgroundColor(APP.Settings.colors.primary || "#000");

	//if this is "the child" of a nother contoller (a details screen, etc), then show the back button in the upper left
	//if(CONFIG.isChild === true) {
	//$.NavigationBar.showBack();
	//if it's not a child and we are using the slide menu, show the slide menu icon in the upper left
	//if(APP.Settings.useSlideMenu) {
	//$.NavigationBar.showMenu();
	//if it's not a child and we aren't using the slide menu, show the settings icon in the navbar
	//} else {
	//$.NavigationBar.showSettings();
	//}
	//}

};

var Cloud = require('ti.cloud');
Cloud.debug = false;

APP.GlobalWrapper.remove(APP.Tabs.Wrapper);
APP.ContentWrapper.bottom = "0dp";

$.signIn.addEventListener('click', function() {
	APP.addChild("login");
});

$.signUp.addEventListener('click', function() {
	APP.addChild("signUp");
});

init();