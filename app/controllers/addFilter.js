var APP = require("core");
var OPTIONS = require("options");
//this is used if you want to pass data from one controller to another
var CONFIG = arguments[0];

//this is the first function of the app that runs
var init = function() {
	//this tracks what the user clicks on and can be sent to us (the developers) if they run into problems
	APP.log("debug", "example.init | " + JSON.stringify(CONFIG));
	OPTIONS.enableBack();
	//set the background color to whatever we set in /app/lib/data/app.json or if we didn't do that, set it to black (#000) by default
	$.NavigationBar.setBackgroundColor(APP.Settings.colors.primary || "#000");

	//if this is "the child" of a nother contoller (a details screen, etc), then show the back button in the upper left
	//if(CONFIG.isChild === true) {
	$.NavigationBar.showBack();
	//if it's not a child and we are using the slide menu, show the slide menu icon in the upper left
	//if(APP.Settings.useSlideMenu) {
	//$.NavigationBar.showMenu();
	//if it's not a child and we aren't using the slide menu, show the settings icon in the navbar
	//} else {
	//	$.NavigationBar.showSettings();
	//}
	//}
};

var Cloud = require('ti.cloud');
Cloud.debug = false;

$.submit.addEventListener('click', function() {
	createFilter();
});

var createFilter = function() {

	Cloud.Objects.create({
		classname: 'filters',
		fields: {
			name: $.filterName.value,
			id: $.filterId.value,
			health: '39%',
			battery: '87%'
		}
	}, function(e) {
		if(e.success) {
			var filter = e.filters[0];
			alert('Success:\n' + 'name: ' + filter.name + '\n' + 'id: ' + filter.id + '\n' + 'health: ' + filter.health + '\n' + 'battery' + filter.battery);
			APP.addChild('settings');
		} else {
			alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}

	});
};

init();